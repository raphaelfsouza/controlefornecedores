@extends('template.head')

@section('content')

<div class="container-fluid">
    <div class="col-md-8">
        <h1 class="page-header ">Forncecedores</h1>
    </div>
    <div class="col-md-4 text-right">
        <a class="btn btn-success" href="{!! route('provider.form') !!}" role="button" title="Editar">
            Novo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </a>
    </div>
</div>  
@if (isset($sucess))
    <div class="alert alert-success">
        Atualizado com sucesso.
    </div>
@endif
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Telefone</th>
        <th>E-mail</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>

      @foreach ($providers as $provider)
          <tr>
              <td>{{ $provider->name }}</td>
              <td>{{ $provider->telefone or '-' }}</td>
              <td>{{ $provider->email }}</td>
              <td>
                  <a class="btn btn-primary" href="{!! route('provider.edit', $provider->id) !!}" role="button" title="Editar">
                      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a>
                  <a class="btn btn-danger" href="{!! route('provider.delete', $provider->id) !!}" role="button" title="Excluir">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                  </a>
              </td>
          </tr>
      @endforeach

    </tbody>
  </table>
</div>
        

@endsection

@extends('template.foot')