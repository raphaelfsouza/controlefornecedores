@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Edição de Fornecedor</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::model($provider, array('route' => array('provider.update'))) !!}
            {{ method_field('PUT') }}
            {{ Form::hidden('idProvider', $provider->id)}}
             <div class="form-group">
                {{ Form::label('name', 'Nome *', array('for' => 'name')) }}
                {{ Form::text('name', $provider->name, array('class' => 'form-control','id' => 'name','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('cpf', 'CPF *', array('for' => 'cpf')) }}
                {{ Form::text('cpf', $provider->cpf, array('class' => 'form-control','id' => 'cpf','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('telefone', 'Telefone *', array('for' => 'telefone')) }}
                {{ Form::text('telefone', $provider->telefone, array('class' => 'form-control','id' => 'telefone','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail *', array('for' => 'email')) }}
                {{ Form::text('email', $provider->email, array('class' => 'form-control','id' => 'email','class' => 'form-control','required') )}}
            </div>
            @if(Auth::user()->permission == 1)
            <div class="form-group">
                {{ Form::label('client', 'Cliente *', array('for' => 'client')) }}
                {{ Form::select('client', $clients, $provider->id_client, ['placeholder' => 'Selecione um Cliente','class' => 'form-control','required']) }}
            </div>
            @endif
            <button type="submit" class="btn btn-primary">Atualizar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')