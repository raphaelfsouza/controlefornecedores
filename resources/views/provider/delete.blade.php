@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Exclusão de Fornecedor</h2>
        {!! Form::open(array('route' => array('provider.destroy', $provider->id))) !!}
            {{ method_field('DELETE') }}
            {{ Form::hidden('idProvider', $provider->id)}}
            <div class="form-group">
                Deseja excluir o cliente <b>{{ $provider->name }} </b>?
            </div>
            
            <button type="submit" class="btn btn-primary">Sim</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')