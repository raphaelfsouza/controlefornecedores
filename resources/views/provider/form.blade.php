@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Cadastro de Fornecedor</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(array('route' => 'provider.save')) !!}
            <div class="form-group">
                {{ Form::label('name', 'Nome *', array('for' => 'name')) }}
                {{ Form::text('name', '', array('class' => 'form-control','id' => 'name','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('cpf', 'CPF *', array('for' => 'cpf')) }}
                {{ Form::text('cpf', '', array('class' => 'form-control','id' => 'cpf','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('telefone', 'Telefone *', array('for' => 'telefone')) }}
                {{ Form::text('telefone', '', array('class' => 'form-control','id' => 'telefone','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail *', array('for' => 'email')) }}
                {{ Form::text('email', '', array('class' => 'form-control','id' => 'email','class' => 'form-control','required') )}}
            </div>
            @if(Auth::user()->permission == 1)
            <div class="form-group">
                {{ Form::label('client', 'Cliente *', array('for' => 'client')) }}
                {{ Form::select('client', $clients, null, ['placeholder' => 'Selecione um Cliente','class' => 'form-control','required']) }}
            </div>
            @endif
            <button type="submit" class="btn btn-primary">Salvar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')