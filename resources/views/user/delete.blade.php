@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Exclusão de Usuário</h2>
        {!! Form::open(array('route' => array('user.destroy', $user->id))) !!}
            {{ method_field('DELETE') }}
            {{ Form::hidden('idUser', $user->id)}}
            <div class="form-group">
                Deseja excluir o usuário <b>{{ $user->name }} </b>?
            </div>
            
            <button type="submit" class="btn btn-primary">Sim</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')