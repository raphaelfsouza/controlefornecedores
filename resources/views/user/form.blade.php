@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Cadastro de Usuário</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(array('route' => 'user.save')) !!}
            <div class="form-group">
                {{ Form::label('name', 'Nome *', array('for' => 'name')) }}
                {{ Form::text('name', '', array('class' => 'form-control','id' => 'name','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail *', array('for' => 'email')) }}
                {{ Form::text('email', '', array('class' => 'form-control','id' => 'email','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('password', 'Senha *', array('for' => 'password')) }}
                {{ Form::password('password', array('class' => 'form-control','id' => 'password','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('permission', 'Permissão *', array('for' => 'permission')) }}
                {{ Form::select('permission', array('0' => 'Comum', '1' => 'Admin'), null, ['placeholder' => 'Selecione uma Permissão','class' => 'form-control','required']) }}
            </div>
            <div class="form-group">
                {{ Form::label('client', 'Cliente *', array('for' => 'client')) }}
                {{ Form::select('client', $clients, null, ['placeholder' => 'Selecione um Cliente','class' => 'form-control','required']) }}
            </div>
            <button type="submit" class="btn btn-primary">Salvar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')