@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Edição de Usuario</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::model($user, array('route' => array('user.update'))) !!}
            {{ method_field('PUT') }}
            {{ Form::hidden('idUser', $user->id)}}
            <div class="form-group">
                {{ Form::label('name', 'Nome *', array('for' => 'name')) }}
                {{ Form::text('name', $user->name, array('class' => 'form-control','id' => 'name','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail *', array('for' => 'email')) }}
                {{ Form::email('email', $user->email, array('class' => 'form-control','id' => 'email','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('permission', 'Permissão *', array('for' => 'permission')) }}
                {{ Form::select('permission', array('0' => 'Comum', '1' => 'Admin'), $user->permission, ['placeholder' => 'Selecione uma Permissão','class' => 'form-control','required']) }}
            </div>
            <div class="form-group">
                {{ Form::label('client', 'Cliente *', array('for' => 'client')) }}
                {{ Form::select('client', $clients, $user->id_client, ['placeholder' => 'Selecione um Cliente','class' => 'form-control','required']) }}
            </div>
            <button type="submit" class="btn btn-primary">Salvar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')