@extends('template.head')

@section('content')

<div class="container-fluid">
    <div class="col-md-8">
        <h1 class="page-header ">Usuários</h1>
    </div>
    <div class="col-md-4 text-right">
        <a class="btn btn-success" href="{!! route('user.form') !!}" role="button" title="Editar">
            Novo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </a>
    </div>
</div>  
@if (isset($sucess))
    <div class="alert alert-success">
        Atualizado com sucesso.
    </div>
@endif
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Permissão</th>
        <th>Cliente</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>

      @foreach ($users as $user)
          <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email or '-' }}</td>
              
              
                @if ($user->permission == 1)
                    <td>Admin</td>
                @else
                    <td>Comum</td>
                @endif
              <td>{{ $user->client->name or '-' }}</td>
              <td>
                  <a class="btn btn-default" href="{!! route('user.uppass', $user->id) !!}" role="button" title="Alterar Senha">
                      <span class="glyphicon glyphicon-tag" aria-hidden="true"></span>
                  </a>
                  <a class="btn btn-primary" href="{!! route('user.edit', $user->id) !!}" role="button" title="Editar">
                      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a>
                  @if ($user->id != 1)
                  <a class="btn btn-danger" href="{!! route('user.delete', $user->id) !!}" role="button" title="Excluir">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                  </a>
                  @endif
              </td>
          </tr>
      @endforeach

    </tbody>
  </table>
</div>
        

@endsection

@extends('template.foot')