@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Edição de Cliente</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(array('route' => 'user.password')) !!}
            {{ method_field('PUT') }}
            {{ Form::hidden('idUser', $user->id)}}
            <div class="form-group">
                {{ Form::label('password', 'Senha *', array('for' => 'password')) }}
                {{ Form::password('password', array('class' => 'form-control','id' => 'password','class' => 'form-control','required') )}}
            </div>
            <button type="submit" class="btn btn-primary">Alterar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')