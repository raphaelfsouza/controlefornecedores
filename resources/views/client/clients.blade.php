@extends('template.head')

@section('content')

<div class="container-fluid">
    <div class="col-md-8">
        <h1 class="page-header ">Clientes</h1>
    </div>
    <div class="col-md-4 text-right">
        <a class="btn btn-success" href="{!! route('client.form') !!}" role="button" title="Editar">
            Novo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </a>
    </div>
</div>  
@if (isset($sucess))
    <div class="alert alert-success">
        Atualizado com sucesso.
    </div>
@endif
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome Fantasia</th>
        <th>Telefone</th>
        <th>E-mail</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>

      @foreach ($clients as $client)
          <tr>
              <td>{{ $client->name }}</td>
              <td>{{ $client->telefone or '-' }}</td>
              <td>{{ $client->email }}</td>
              <td>
                  <a class="btn btn-primary" href="{!! route('client.edit', $client->id) !!}" role="button" title="Editar">
                      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a>
                  <a class="btn btn-danger" href="{!! route('client.delete', $client->id) !!}" role="button" title="Excluir">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                  </a>
              </td>
          </tr>
      @endforeach

    </tbody>
  </table>
</div>
        

@endsection

@extends('template.foot')