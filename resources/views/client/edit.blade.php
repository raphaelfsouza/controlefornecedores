@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Edição de Cliente</h2>
        
        @if ($errors->has())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::model($client, array('route' => array('client.update'))) !!}
            {{ method_field('PUT') }}
            {{ Form::hidden('idCliente', $client->id)}}
            <div class="form-group">
                {{ Form::label('nomeFantasia', 'Nome Fantasia *', array('for' => 'nomeFantasia')) }}
                {{ Form::text('nomeFantasia', $client->name, array('class' => 'form-control','id' => 'nomeFantasia','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('cnpj', 'CNPJ *', array('for' => 'cnpj')) }}
                {{ Form::text('cnpj', $client->cnpj, array('class' => 'form-control','id' => 'cnpj','class' => 'form-control','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('logradouro', 'Logradouro', array('for' => 'logradouro')) }}
                {{ Form::textarea('logradouro', $client->logradouro, array('class' => 'form-control','id' => 'logradouro','class' => 'form-control','rows'=>'4','required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('telefone', 'Telefone *', array('for' => 'telefone')) }}
                {{ Form::text('telefone', $client->telefone, array('class' => 'form-control','id' => 'telefone','class' => 'form-control' ,'required') )}}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'E-mail', array('for' => 'email')) }}
                {{ Form::text('email', $client->email, array('class' => 'form-control','id' => 'email','class' => 'form-control','required') )}}
            </div>
            <button type="submit" class="btn btn-primary">Atualizar</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')