@extends('template.head')

@section('content')

    <div class="container-fluid">
        <h2>Exclusão de Cliente</h2>
        {!! Form::open(array('route' => array('client.destroy', $client->id))) !!}
            {{ method_field('DELETE') }}
            {{ Form::hidden('idCliente', $client->id)}}
            <div class="form-group">
                Deseja excluir o cliente <b>{{ $client->name }} </b>?
            </div>
            
            <button type="submit" class="btn btn-primary">Sim</button>
        {!! Form::close() !!}
    </div>

@endsection

@extends('template.foot')