<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Providers;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Auth;
use Validator;

class ProviderController extends Controller
{
    public function index()
    {
        if(Auth::user()->permission == 1){
            $providers = \App\Providers::all();
        }else{
            $providers = \App\Providers::where('id_client', Auth::user()->id_client)->get();
        }
        
        return view('provider/providers',['providers' => $providers]);
    }
    
    public function show()
    {
        $clients = \App\Clients::orderBy('name', 'asc')->get();
        $c = array();
        foreach ($clients as $cli){
            $c[$cli->id] = $cli->name;
        }
        return view('provider/form',['clients' => $c]);
    }
    
    public function edit($id)
    {
        $provider = Providers::find($id);
        $clients = \App\Clients::orderBy('name', 'asc')->get();
        foreach ($clients as $cli){
            $c[$cli->id] = $cli->name;
        }
        return view('provider/edit',['clients' => $c,'provider' => $provider]);
    }
    
    public function saveProvider(Request $request)
    {
        $messages = [
            'name.required' => 'O Nome é necessario.',
            'required' => 'O :attribute é necessario.',
            'unique' => 'O :attribute já existe.',
        ];
        
        $validator  = Validator::make($request->all(), [
            'name' => 'required',
            'email' => "required",
        ],$messages);
        
        if($validator->fails()){
            return redirect()->route('provider.edit', $request->idUser)
                    ->withErrors($validator)
                    ->withInput();
        }{
            $provider = new \App\Providers();
            $provider->name = $request->name;
            $provider->cpf = $request->email;
            $provider->telefone = $request->telefone;
            $provider->email = $request->email;
            if(Auth::user()->permission == 1){
                $provider->id_client = $request->client;
            }else{
                $provider->id_client = Auth::user()->id_client;
            }

            $result = $provider->save();
            if($result){
               return Redirect::route('provider.all', array('sucess' => 'Atualizado com Sucesso'));
            }else{
                return view('provider/form', ['updateError' => 'erro'])->withInput();
            }
        }
    }
    
    public function updateProvider(Request $request)
    {
        $messages = [
            'name.required' => 'O Nome é necessario.',
            'required' => 'O :attribute é necessario.',
            'unique' => 'O :attribute já existe.',
        ];
        
        $validator  = Validator::make($request->all(), [
            'name' => 'required',
            'email' => "required",
        ],$messages);
        
        if($validator->fails()){
            return redirect()->route('provider.edit', $request->idUser)
                    ->withErrors($validator)
                    ->withInput();
        }{
            $provider = \App\Providers::find($request->idProvider);
            $provider->name = $request->name;
            $provider->cpf = $request->email;
            $provider->telefone = $request->telefone;
            $provider->email = $request->email;
            if(Auth::user()->permission == 1){
                $provider->id_client = $request->client;
            }else{
                $provider->id_client = Auth::user()->id_client;
            }

            $result = $provider->save();
            if($result){
               return Redirect::route('provider.all', array('sucess' => 'Atualizado com Sucesso'));
            }else{
               return view('provider/edit', ['updateError' => 'erro'])->withInput();
            }
        }
    }
    
    public function deleteProvider($id)
    {
        $provider = \App\Providers::find($id);
        return view('provider/delete', ['provider' => $provider]);
    }
    
    public function destroyProvider(Request $request)
    {
        $result = \App\Providers::destroy($request->idProvider);
        if($result){
            return redirect()->route('provider.all');
        }else{
            return redirect()->route('provider.all');
        }
    }
}
