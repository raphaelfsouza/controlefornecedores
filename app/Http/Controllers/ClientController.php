<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use App\Clients;
use Validator;

class ClientController extends Controller
{
    public function index()
    {
        $clients = \App\Clients::all();
        return view('client/clients',['clients' => $clients]);
    }
    
    public function show()
    {
        return view('client/form');
    }
    
    public function edit($id)
    {
        $client = \App\Clients::find($id);
        
        return view('client/edit', ['client' => $client]);
    }
    
    public function saveClient(Request $request)
    {   
        $messages = [
            'required' => 'O :attribute é necessario.',
            'unique' => 'O :attribute já existe.',
        ];
        
        $validator  = Validator::make($request->all(), [
            'nomeFantasia' => 'required',
            'cnpj' => 'required|unique:clients,cnpj',
            'email' => 'required|unique:clients,email',
            'telefone' => 'required',
        ],$messages);
        
        if($validator->fails()){
            return redirect()->route('client.form', $request->idCliente)
                    ->withErrors($validator)
                    ->withInput();
        }{
            $client = new Clients();

            $client->name = $request->input('nomeFantasia');
            $client->cnpj = $request->input('cnpj');
            $client->logradouro = $request->input('logradouro');
            $client->telefone = $request->input('telefone');
            $client->email = $request->input('email');

            if($client->save()){
                return redirect()->route('client.all');
            }else{
                return redirect()->route('client.form', $request->idCliente)
                    ->withErrors($validator)
                    ->withInput();
            }
        }
    }
    
    public function updateClient(Request $request)
    {   
        $messages = [
            'name.required' => 'O Nome Fantasia é necessario.',
            'required' => 'O :attribute é necessario.',
            'unique' => 'O :attribute já existe.',
        ];
        
        $validator  = Validator::make($request->all(), [
            'nomeFantasia' => 'required',
            'cnpj' => "required|unique:clients,cnpj,{$request->idCliente},id",
            'email' => "required|unique:clients,email,{$request->idCliente},id",
            'telefone' => 'required',
        ],$messages);
        
        if($validator->fails()){
            return redirect()->route('client.edit', $request->idCliente)
                    ->withErrors($validator)
                    ->withInput();
        }{
            $client = \App\Clients::find($request->idCliente);

            $client->name = $request->input('nomeFantasia');
            $client->cnpj = $request->input('cnpj');
            $client->logradouro = $request->input('logradouro');
            $client->telefone = $request->input('telefone');
            $client->email = $request->input('email');

            $result = $client->save();
            if($result){
               return Redirect::route('client.all', array('sucess' => 'Atualizado com Sucesso'));
            }else{
               return view('client/edit', ['updateError' => 'erro'])->withInput();
            }
        }
    }
    
    public function deleteClient($id)
    {   
        $client = \App\Clients::find($id);
        return view('client/delete', ['client' => $client]);
    }
    
    public function destroyClient(Request $request)
    {   
        //return $request->idCliente;
        $result = \App\Clients::destroy($request->idCliente);
        if($result){
            return redirect()->route('client.all');
        }else{
            return redirect()->route('client.all');
        }
    }
}
