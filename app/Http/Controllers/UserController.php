<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = \App\User::all();
        var_dump($users);
        return  $users;
    }
    
    public function users()
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $users = \App\User::all();
        return view('user/users',['users' => $users]);
    }
    
    public function show()
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $clients = \App\Clients::orderBy('name', 'asc')->get();
        $c = Array();
        foreach ($clients as $cli){
            $c[$cli->id] = $cli->name;
        }
        return view('user/form',['clients' => $c]);
    }
    
    public function edit($id)
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $clients = \App\Clients::orderBy('name', 'asc')->get();
        
        foreach ($clients as $cli){
            $c[$cli->id] = $cli->name;
        }
        
        $user = \App\User::find($id);
        
        return view('user/edit',['clients' => $c,'user' => $user]);
    }
    
    public function uppass($id)
    {
        $user = \App\User::find($id);
        return view('user/passedit', ['user' => $user]);
    }
    
    public function updatePassword(Request $request){
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        
        $user = \App\User::find($request->idUser);
        $user->password = bcrypt($request->input('password'));

        $result = $user->save();
        if($result){
           return Redirect::route('user.all', array('sucess' => 'Atualizado com Sucesso'));
        }else{
           return view('user/edit', ['updateError' => 'erro'])->withInput();
        }
    }
    
    public function saveUser(Request $request)
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $user = new \App\User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->permission = $request->permission;
        $user->id_client = $request->client;
        
        $user->save();
        
        return redirect()->route('user.all');
    }
    
    public function updateUser(Request $request)
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        
        $messages = [
            'name.required' => 'O Nome é necessario.',
            'required' => 'O :attribute é necessario.',
            'unique' => 'O :attribute já existe.',
        ];
        
        $validator  = Validator::make($request->all(), [
            'name' => 'required',
            'email' => "required|unique:clients,email,{$request->idUser},id",
        ],$messages);
        
        if($validator->fails()){
            return redirect()->route('user.edit', $request->idUser)
                    ->withErrors($validator)
                    ->withInput();
        }{
            $user = \App\User::find($request->idUser);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->permission = $request->input('permission');
            $user->id_client = $request->input('client');

            $result = $user->save();
            if($result){
               return Redirect::route('user.all', array('sucess' => 'Atualizado com Sucesso'));
            }else{
               return view('user/edit', ['updateError' => 'erro'])->withInput();
            }
        }
        return view('user/form');
    }
    
    public function deleteUser($id)
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $user = \App\User::find($id);
        return view('user/delete', ['user' => $user]);
    }
    
    public function destroyUser(Request $request)
    {
        if(Auth::user()->permission != 1){
            return redirect()->route('dash');
        }
        $result = \App\User::destroy($request->idUser);
        if($result){
            return redirect()->route('user.all');
        }else{
            return redirect()->route('user.all');
        }
    }
    
}
