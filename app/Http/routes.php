<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['middleware' => 'auth', 'as' => 'dash', function () {
    return view('dash');
}]);

//Users
Route::get('/users', ['middleware' => 'auth', 'as' => 'user.all', 'uses' => 'UserController@users']);
Route::get('/user', ['middleware' => 'auth', 'as' => 'user.form', 'uses' => 'UserController@show']);
Route::get('/user/{id}', ['middleware' => 'auth', 'as' => 'user.edit', 'uses' => 'UserController@edit']);
Route::get('/user/uppass/{id}', ['middleware' => 'auth', 'as' => 'user.uppass', 'uses' => 'UserController@uppass']);
Route::put('/user/uppass', ['middleware' => 'auth', 'as' => 'user.password', 'uses' => 'UserController@updatePassword']);
Route::post('/user', ['middleware' => 'auth', 'as' => 'user.save', 'uses' => 'UserController@saveUser']);
Route::put('/user', ['middleware' => 'auth', 'as' => 'user.update', 'uses' => 'UserController@updateUser']);
Route::get('/user/delete/{id}', ['middleware' => 'auth', 'as' => 'user.delete', 'uses' => 'UserController@deleteUser']);
Route::delete('/user/destroy', ['middleware' => 'auth', 'as' => 'user.destroy', 'uses' => 'UserController@destroyUser']);

//Clientes
Route::get('/clients', ['middleware' => 'auth', 'as' => 'client.all', 'uses' => 'ClientController@index']);
Route::get('/client', ['middleware' => 'auth', 'as' => 'client.form', 'uses' => 'ClientController@show']);
Route::get('/client/{id}', ['middleware' => 'auth', 'as' => 'client.edit', 'uses' => 'ClientController@edit']);
Route::post('/client', ['middleware' => 'auth', 'as' => 'client.save', 'uses' => 'ClientController@saveClient']);
Route::put('/client', ['middleware' => 'auth', 'as' => 'client.update', 'uses' => 'ClientController@updateClient']);
Route::get('/client/delete/{id}', ['middleware' => 'auth', 'as' => 'client.delete', 'uses' => 'ClientController@deleteClient']);
Route::delete('/client/destroy', ['middleware' => 'auth', 'as' => 'client.destroy', 'uses' => 'ClientController@destroyClient']);

Route::get('/providers', ['middleware' => 'auth', 'as' => 'provider.all', 'uses' => 'ProviderController@index']);
Route::get('/provider', ['middleware' => 'auth', 'as' => 'provider.form', 'uses' => 'ProviderController@show']);
Route::get('/provider/{id}', ['middleware' => 'auth', 'as' => 'provider.edit', 'uses' => 'ProviderController@edit']);
Route::post('/provider', ['middleware' => 'auth', 'as' => 'provider.save', 'uses' => 'ProviderController@saveProvider']);
Route::put('/provider', ['middleware' => 'auth', 'as' => 'provider.update', 'uses' => 'ProviderController@updateProvider']);
Route::get('/provider/delete/{id}', ['middleware' => 'auth', 'as' => 'provider.delete', 'uses' => 'ProviderController@deleteProvider']);
Route::delete('/provider/destroy', ['middleware' => 'auth', 'as' => 'provider.destroy', 'uses' => 'ProviderController@destroyProvider']);

Route::auth();

